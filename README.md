# NukeBot
A simple discord bot for nuke code shenanigans.
To use for yourself, add a file to the main directory called `nuke.token` with your bot token and guild formatted as:
```
TOKEN XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
GUILD XXXXXXXXXXXXXXXXXX
```

Prefix: >

## Commands:

`nukemedaddy` Produces a random doujin

`gib <code>` Shows what the nuke code produces

`gib <tags>` gives a random doujin with specified tags

`cursedtags` lists out all the cursed tags in chat

`blessedtags` lists out all the blessed tags in chat

`checkcurse` will show you how many cursed points you have accrued this month

`checkbless` will show you how many blessed points you have accrued this month

`checktomboy` will show you how many tomboy points you have accrued this month

`topcurse` will show the top three most cursed persons on the server

`topbless` will show the top three most blessed persons on the server

`toptomboy` will show the top three tomboy magnets on the server


## Roles

Cursed Kang, The most cursed person the previous month.

Blessed Kang, The most blessed person the previous month.

Tomboy Kang, The biggest tomboy magnet on the server.
